﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Den.Tools;
using Den.Tools.GUI;
using Den.Tools.Lines;
using Den.Tools.Matrices;

using Den.Tools.Tests;

using MapMagic.Nodes;
using MapMagic.Terrains;
using MapMagic.Core;
using MapMagic.Products;

namespace MapMagic.Nodes.Tests
{
	public interface ITestInput
	{  
		object Read (CoordRect pixelRect, Vector2D worldPos, Vector2D worldSize, float worldHeight); 
	}


	public interface ITestOutput
	{  
		void Write (object result);
	}


	[System.Serializable]
	public class TextureInput : ITestInput
	{
		[Val("Texture")] public Texture2D texture;
		[Val("Rescale")] public bool rescale = true;
		//[Val("Tile Mode")] public CoordRect.TileMode tileMode = CoordRect.TileMode.Clamp;

		public object Read (CoordRect pixelRect, Vector2D worldPos, Vector2D worldSize, float worldHeight)
		{
			if (texture==null) return null;

			CoordRect texRect = new CoordRect( 0,0,texture.width, texture.height);
			MatrixWorld texMatrix = new MatrixWorld(texRect, worldPos, worldSize, worldHeight);
			texMatrix.ImportTexture(texture, 0);

			MatrixWorld mw = new MatrixWorld(pixelRect, worldPos, worldSize, worldHeight);

			if (rescale)
				MatrixOps.Resize(texMatrix, mw);
			else
				Matrix.ReadMatrix(texMatrix, mw);

			return mw;
		}
	}
 

	[System.Serializable]
	public class ChildrenIn : ITestInput
	{
		[Val("Parent", allowSceneObject = true)] public Transform parent;
		[Val("Resolution")] public int resolution = 64;

		public object Read (CoordRect pixelRect, Vector2D worldPos, Vector2D worldSize, float worldHeight)
		{
			int childCount = parent.childCount;
			TransitionsList trns = new TransitionsList();

			for (int c=0; c<childCount; c++)
			{
				Transform child = parent.GetChild(c);
				Transition trs = new Transition(child.position.Vector3D(), child.rotation.Vector4D(), child.localScale.Vector3D(), child.GetInstanceID());
				trns.Add(trs);
			}

			return trns;
		}
	}


	[System.Serializable]
	public class PositionListIn : ITestInput, ISceneGUI
	{
		[Val("Count")] public int count;
		[Val("Positions")] public Vector3[] positions;

		public object Read (CoordRect pixelRect, Vector2D worldPos, Vector2D worldSize, float worldHeight)
		{
			if (positions == null) return null;

			TransitionsList trns = new TransitionsList();

			for (int i=0; i<positions.Length; i++)
			{
				Transition trs = new Transition(positions[i].x, positions[i].y, positions[i].z);
				trns.Add(trs);
			}

			return trns;
		}

		public void OnSceneGUI ()
		{
			if (positions==null)
				positions = new Vector3[count];

			if (positions.Length!=count)
				positions = ArrayTools.Resize(positions, count);

			for (int i=0; i<positions.Length; i++)
				positions[i] = DebugGizmos.PositionHandle( positions[i] );
		}
	}


	[System.Serializable]
	public class ChildrenOut : ITestOutput
	{
		[Val("Parent", allowSceneObject = true)] public Transform parent;
		[Val("Object Prefab")] public Transform objPrefab;

		public void Write (object result)
		{
			TransitionsList trns = (TransitionsList)result;
			int childCount = parent.childCount;

			int c=0;
			for (int t=0; t<trns.count; t++)
			{
				Transform tfm;
				if (c >= childCount)
				{
					if (objPrefab == null) tfm = GameObject.CreatePrimitive(PrimitiveType.Sphere).transform;
					else tfm = GameObject.Instantiate(objPrefab);
				}
				else
					tfm = parent.GetChild(c);

				tfm.position = trns.arr[t].pos.Vector3();
				tfm.localScale = trns.arr[t].scale.Vector3();
				tfm.rotation = trns.arr[t].rotation.Quaternion();

				tfm.parent = parent;

				c++;
			}
		}
	}



	[System.Serializable]
	public class GizmosOut : ITestOutput
	{
		[Val("Color")] public Color color = Color.red;
		[Val("Size")] public float size = 1;

		[NonSerialized] private TransitionsList trns;

		public void Write (object result)
		{
			trns = (TransitionsList)result;
					
			#if UNITY_EDITOR
			#if UNITY_2019_1_OR_NEWER
			UnityEditor.SceneView.duringSceneGui -= DrawGizmos;
			UnityEditor.SceneView.duringSceneGui += DrawGizmos;
			#else
			UnityEditor.SceneView.onSceneGUIDelegate -= DrawGizmos;
			UnityEditor.SceneView.onSceneGUIDelegate += DrawGizmos;
			#endif
			#endif
		}

		#if UNITY_EDITOR
		public void DrawGizmos (UnityEditor.SceneView sceneView)
		{
			UnityEditor.Handles.color = color;

			for (int t=0; t<trns.count; t++)
			{
				Vector3 pos = trns.arr[t].pos.Vector3();
				Vector3 scale = trns.arr[t].scale.Vector3();  

				UnityEditor.Handles.CubeHandleCap(0, pos, Quaternion.identity, size, EventType.Repaint);
			}

			/*UnityEditor.Handles.color = new Color(color.r, color.g, color.b, color.a*0.1f);
			for (int c=0; c<posTab.cells.arr.Length; c++)
			{
				CoordRect rect = posTab.cells.arr[c].rect;
				UnityEditor.Handles.DrawLine(rect.offset.vector3, (rect.offset+new Coord(0, rect.size.z)).vector3);
				UnityEditor.Handles.DrawLine((rect.offset+new Coord(0, rect.size.z)).vector3, (rect.offset+new Coord(rect.size.x, rect.size.z)).vector3);
				UnityEditor.Handles.DrawLine((rect.offset+new Coord(rect.size.x, rect.size.z)).vector3, (rect.offset+new Coord(rect.size.x, 0)).vector3);
				UnityEditor.Handles.DrawLine((rect.offset+new Coord(rect.size.x, 0)).vector3, (rect.offset).vector3);
			}*/
		}
		#endif
	}


	[System.Serializable]
	public class SplineInout : ITestInput, ITestOutput
	{
		[Val("Spline", allowSceneObject=true)] public LineObject splineObj; 

		public object Read (CoordRect pixelRect, Vector2D worldPos, Vector2D worldSize, float worldHeight)
		{
			if (splineObj == null) return null;
			else return new LineSys(splineObj.lines);
		}

		public void Write (object result)
		{
			if (result != null)
				splineObj.lines = ((LineSys)result).splines;
		}
	}


	[System.Serializable]
	public class SplineGizmoOut : ITestOutput, ISceneGUI
	{
		public LineSys splineSys; 

		public void Write (object result) => splineSys = (LineSys)result;
	
		public void OnSceneGUI ()
		{
			DebugGizmos.Clear("SplineGizmoTest");

			if (splineSys!=null)
				for (int i=0; i<splineSys.splines.Length; i++)
					DebugGizmos.DrawPolyLine("SplineGizmoTest",  Array.ConvertAll(splineSys.splines[i].nodes, n=>n.Vector3()), Color.green, additive:true);
		}
	}


	[System.Serializable]
	public class TerrainInout : ITestInput, ITestOutput
	{
		[Val("Terrain", allowSceneObject=true)] public Terrain terrain;

		public object Read (CoordRect pixelRect, Vector2D worldPos, Vector2D worldSize, float worldHeight)
		{
			MatrixWorld matrix = new MatrixWorld(pixelRect, worldPos, worldSize, worldHeight);

			float[,] arr2D = terrain.terrainData.GetHeights(0,0,terrain.terrainData.heightmapResolution, terrain.terrainData.heightmapResolution);
			matrix.ImportHeights(arr2D);

			return matrix;
		}

		public void Write (object result)
		{
			MatrixWorld matrix = (MatrixWorld)result;
			if (terrain == null) return;

			int terrainRes = Mathf.NextPowerOfTwo(matrix.rect.size.x-1) + 1;
			float resFactor = 1f * terrainRes / matrix.rect.size.x;

			if (terrain.terrainData.heightmapResolution != terrainRes)
				terrain.terrainData.heightmapResolution = terrainRes;

			terrain.transform.position = matrix.worldPos.Vector3();
			terrain.terrainData.size = new Vector3(matrix.worldSize.x*resFactor, matrix.worldSize.y, matrix.worldSize.z*resFactor); //since terrainRes is usually about 2 times bigger

			float[,] arr2D = new float[terrainRes, terrainRes];
			matrix.ExportHeights(arr2D);
			terrain.terrainData.SetHeights(0,0,arr2D);
		}
	}


	[System.Serializable]
	public class TerrainSplatsInout : ITestInput, ITestOutput
	{
		//[Val("Terrain", allowSceneObject = true)] 
		[Val("Terrain", allowSceneObject=true)] public Terrain terrain;
		[Val("Channel")] public int channel;

		public object Read (CoordRect pixelRect, Vector2D worldPos, Vector2D worldSize, float worldHeight)
		{
			MatrixWorld matrix = new MatrixWorld(pixelRect, worldPos, worldSize, worldHeight);
			matrix.ImportData(terrain.terrainData, channel);
			return matrix;
		}

		public void Write (object result)
		{
			MatrixWorld matrix = (MatrixWorld)result;
			matrix.ExportTerrainData(terrain.terrainData, channel);
		}
	}


	[System.Serializable]
	public class ScatterIn : ITestInput
	{
		[Val("Count")] public int count;
		[Val("Uniformity")] public float uniformity;
		[Val("Seed")] public int seed = 12345;
		[Val("Relax")] public float relax = 0.1f;

		public object Read (CoordRect pixelRect, Vector2D worldPos2D, Vector2D worldSize2D, float worldHeight)
		{
			Vector3D worldPos = worldPos2D.Vector3D();
			Vector3D worldSize = worldSize2D.Vector3D();
			worldSize.y = worldHeight;
			int cellSize =  (int)Mathf.Sqrt(count);

			CoordRect rect = CoordRect.WorldToPixel(worldPos.Vector2D(), worldSize.Vector2D(), (Vector2D)cellSize);  //will convert to cells as well
			worldPos = new Vector3D(rect.offset.x*cellSize, worldPos.y, rect.offset.z*cellSize);  //modifying worldPos/size too
			worldSize = new Vector3D(rect.size.x*cellSize, worldSize.y, rect.size.z*cellSize);

			Noise random = new Noise(seed, permutationCount:65536);
return null;
/*
//TODO: positionmatrix to native
			PositionMatrix posMatrix = new PositionMatrix(rect, worldPos, worldSize);
			posMatrix.Scatter(uniformity, random);
			posMatrix = posMatrix.Relaxed(relax);

			return posMatrix.ToTransitionsList();
*/
		}
	}


	[System.Serializable]
	public class MatrixGizmoTextureOut : ITestOutput, ISceneGUI 
	{
		MatrixTextureGizmo gizmo;
		[Val("Center Cell")] public bool centerCell = true;
		[Val("Filter Mode")] public FilterMode filterMode;

		public void Write (object result)
		{
			MatrixWorld matrix = (MatrixWorld)result;
			
			if (gizmo == null) gizmo = new MatrixTextureGizmo();
			gizmo.SetMatrixWorld(matrix, centerCell, filterMode);
		}

		public void OnSceneGUI ()
		{
			gizmo?.Draw();
		}
	}


	[System.Serializable]
	public class MatrixGizmoHeightOut : ITestOutput, ISceneGUI 
	{
		MatrixHeightGizmo gizmo;
		[Val("Faceted")] public bool faceted = true;

		public void Write (object result)
		{
			MatrixWorld matrix = (MatrixWorld)result;
			
			if (gizmo == null) gizmo = new MatrixHeightGizmo();
			gizmo.SetMatrixWorld(matrix, faceted:faceted);
		}

		public void OnSceneGUI ()
		{
			gizmo?.Draw();
		}
	}


	[System.Serializable]
	public class MatrixObjectInout : ITestOutput, ITestInput 
	{
		[Val("Object", allowSceneObject=true)] public MatrixObject obj;
		[Val("Rescale")] public bool rescale = true;

		public object Read (CoordRect pixelRect, Vector2D worldPos, Vector2D worldSize, float worldHeight)
		{
			if (obj?.matrix==null) return null;

			MatrixWorld mw = new MatrixWorld(pixelRect, worldPos, worldSize, worldHeight);

			if (rescale)
				MatrixOps.Resize(obj.matrix, mw);
			else
				Matrix.ReadMatrix(obj.matrix, mw);

			obj.worldPosition = worldPos;
			obj.worldSize = worldSize;
			obj.worldHeight = worldHeight;
			//to make obj preview up to date

			return mw;
		}

		public void Write (object result)
		{
			if (obj == null) return;
			MatrixWorld mw = (MatrixWorld)result;
			obj.worldPosition = mw.worldPos.Vector2D();
			obj.worldSize = mw.worldSize.Vector2D();
			obj.worldHeight = mw.worldPos.y;
			obj.matrix = mw;
		}
	}
}
